<div class="searchform">
<form method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>/" role="search">
	<?php $form_id = rand( 100, 999 ); ?>
	<p>
	<label for="s<?php echo $form_id; ?>" class='screen-reader-text'>Suche nach:</label>
	<div class="searchboxform">
		<input type="text" name="s" class="searchboxforminput" id="s<?php echo $form_id; ?>" placeholder="......" value="<?php echo trim( get_search_query() ); ?>" />
		<input type="submit" name="submit" value="suchen" class="button" />
	</div>
	</p>
</form>
</div>
