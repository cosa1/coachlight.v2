<?php
if ( ! is_active_sidebar( 'sidebar-3' ) ) {


	return;
}
?>
<div class="">
	<div id="partner" class="widget-area" role="complementary">
		<?php dynamic_sidebar( 'sidebar-3' ); ?>
	</div><!-- #secondary -->
</div><!-- .col-md-4>-->
