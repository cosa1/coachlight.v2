<?php
/* Footer Class wird per Script in Zeile 19 ersetzt, damit Footer-Container auf der selben Ebene wie Navbar und Inhalt erscheint. */
 ?>


<div class="container-fluid hide" id="footer-container" style=" <?php if(!empty(get_theme_mod('footer_size'))){ echo 'height:'.get_theme_mod('footer_size').'px;';}?> <?php  if(!empty(get_theme_mod('footer_bg_color'))){ echo 'background-color:'.get_theme_mod('footer_bg_color'); };?>">
        <div class="row rowfooter">
          <div class="col-12 col-md-12 col-lg-12 col-xl-12">
            <?php get_sidebar('footer'); ?>
          </div>
        </div> <!-- #row -->
</div>
        </div> <!-- main col -->
    <?php wp_footer(); ?>
    <script>
      document.getElementById('allcontent').appendChild(document.getElementById('footer-container'));
      document.getElementById('footer-container').className = "container-fluid mt-auto";
    </script>
  </body>
</html>
