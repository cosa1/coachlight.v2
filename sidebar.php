<?php
/*if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}*/
if (! is_active_sidebar( 'sidebar-1' )) :
?>
<?php else:?>

	<div class="col-1 col-md-1 d-lg-none d-xl-none sidebar">

	</div><!-- .col-md-4>-->

	<div class="d-none d-sm-none col-lg-2 col-xl-2 sidebar paddingzero">
		<div id="first" class="widget-area" role="complementary">
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
		</div><!-- #secondary -->
	</div><!-- .col-md-4>-->

<?php endif; ?>
