<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?php wp_title( '|', true, 'right' ); bloginfo('name'); ?></title>
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <style>
    nav.navbar.navbar-default {
      min-height: <?php echo get_theme_mod('header_navbar_size');?>px;
      padding-top: <?php echo (intval(get_theme_mod('header_navbar_size'))-50)/2;?>px;
    }
    .intro-header {
      margin-top: <?php echo -(intval(get_theme_mod('header_navbar_size')));?>px !important;
    }

    <?php
    if(!empty(get_theme_mod('header_font_color'))) {
    ?>
    #menu-hauptmenue a{
      color: <?php echo get_theme_mod('header_font_color');?>;
    }
    <?php
    }
    ?>

    <?php
    if(!empty(get_theme_mod('header_bg_color'))) {
    ?>
    .navbar .dropdown-menu{
      background-color: <?php echo get_theme_mod('header_bg_color');?>!important;
    }
    <?php
    }
    ?>

  </style>
  <?php
  // Fires the 'wp_head' action and gets all the scripts included by wordpress, wordpress plugins or functions.php
  // using wp_enqueue_script if it has $in_footer set to false (which is the default)
  wp_head(); ?>
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/respond.min.js"></script>
  <![endif]-->
</head>
<body <?php body_class(); ?> style="background-color:<?php echo get_theme_mod('body_bg_color');?>">
<div id="allcontent" class="d-flex flex-column" style="padding-bottom: <?php echo get_theme_mod('footer_size');?>px">
<nav class="navbar navbar-expand-lg navbar-light mb-5" <?php  if(!empty(get_theme_mod('header_bg_color'))){ echo 'style=background-color:'.get_theme_mod('header_bg_color'); }?> >
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <a class="navbar-brand" href="<?php echo home_url(); ?>">
      <img alt="Titel" src="<?php echo get_header_image();?>">
    </a>

    <button class="navbar-toggle d-lg-none py-2" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-controls="bs-example-navbar-collapse-1" aria-expanded="false" aria-label="Toggle navigation">
        <!-- <span class="navbar-toggler-icon"></span> -->
        <span class="icon-bar mb-2"></span>
        <span class="icon-bar mb-2"></span>
        <span class="icon-bar mb-0"></span>
    </button>
        <?php
        wp_nav_menu( array(
            'theme_location'    => 'primary-menu',
            'depth'             => 2,
            'container'         => 'div',
            'container_class'   => 'collapse navbar-collapse',
            'container_id'      => 'bs-example-navbar-collapse-1',
            'menu_class'        => 'nav navbar-nav',
            'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
            'walker'            => new WP_Bootstrap_Navwalker(),
        ) );
        ?>
    </div>
</nav>
<div class="container-fluid">
<div class="row"> <!-- main row -->
  <div class="col-12 col-md-12 col-lg-12 col-xl-12"> <!-- main col -->
    <!--<div class="row">
      <div class="colpaddingzero">
        <div class="container-fluid">
         <div class="row">
        	  <img id ="eregiowerk_headerimg" src="<?php echo get_header_image();?>" class="img-responsive center-block" width="100%">
              <div class="pagelogo"><?php the_custom_logo();?>
         </div>
       </div>
     </div>
    </div>

    </div>-->


   <div class="row">
     <!--<div class="hidden-xs col-xs-10 col-xs-offset-1  col-md-12 col-md-offset-0 col-lg-8 col-lg-offset-2 breadcrumbsrow">-->
       <div class="hidden-xs breadcrumbsrow">
       <div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
           <?php if(function_exists('bcn_display'))
           {
               bcn_display();
           }?>
       </div>
     </div>
  </div>
