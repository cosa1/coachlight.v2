<?php
if ( ! is_active_sidebar( 'sidebar-2' ) ) {


	return;
}
?>
<div class="col-lg-3 col-xl-3 colsidebarright">
	<div id="secondary" class="widget-area" role="complementary">
		<?php dynamic_sidebar( 'sidebar-2' ); ?>
	</div><!-- #secondary -->
</div><!-- .col-md-4>-->
