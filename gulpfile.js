/**
 * Gulpfile for the coachlight Theme. *
 *
 * @since 1.0.0
 * @author Max Sternitzke
 */

/**
 * Load Plugins.
 *
 * Load gulp plugins and assing them semantic names.
 */
var gulp         = require('gulp'); // Gulp of-course

// CSS related plugins.
var sass         = require('gulp-sass')(require('sass')); // Gulp pluign for Sass compilation
var sassThemes   = require('gulp-sass-themes');

// Utility related plugins.
var notify       = require('gulp-notify'); // Sends message notification to you
var zip          = require('gulp-zip');
var clean        = require('gulp-clean');
var once         = require('gulp-once');


var outputfolder ="./output/";
var releasefolder = outputfolder + "coachlightv2/";
var styleBasic = "./sass/style.themed.scss";
var styleThemes= "./sass/themes/_*.scss";
var styleReleaseFolder = releasefolder + "css/";
var styleDevFolder = "./css/";

 gulp.task('cleanrelease', function() {
   return gulp.src(outputfolder, {read: false, allowEmpty: true })
      .pipe(once())
      .pipe(clean());
 });

 gulp.task( 'copyfiles', gulp.series('cleanrelease', function () {
  return gulp.src( [
    "./customizer-alpha-color-picker/**",
    "./fonts/**",
    "./images/**",
    "./inc/**",
    "./js/**",
    "./vendor/**",
    "./template-parts/**",
    "./readme.txt",
    "./*.php",
    "./screenshot.png",
    "./gpl-3.txt",
    "./style.css",
    ] , {
    base: './'
  })
  .pipe(gulp.dest(releasefolder))
  .pipe( notify( { message: 'TASK: "copyfiles" Completed!', onLast: true } ) );
}));

 
 gulp.task('styles', function () {
    return gulp.src(styleBasic)
    .pipe(sassThemes(styleThemes))
    .pipe(sass()).on('error', sass.logError)
    .pipe(gulp.dest(styleReleaseFolder))
    .pipe( notify( { message: 'TASK: "styles" Completed!', onLast: true }))
 });



gulp.task('buildzip', function () {
  return gulp.src(outputfolder+'/**')
       .pipe(zip('coachlightv2.zip'))
       .pipe(gulp.dest(outputfolder))
       .pipe( notify( { message: 'TASK: "buildzip" Completed!', onLast: true } ) );
});

gulp.task('cleancss', function () {
  return gulp.src(styleDevFolder, {read: false, allowEmpty: true })
    .pipe(once())
    .pipe(clean());
});

gulp.task('compilestyles', gulp.series('cleancss', function() {
  return gulp.src(styleBasic)
  .pipe(sassThemes(styleThemes))
  .pipe(sass()).on('error', sass.logError)
  .pipe(gulp.dest(styleDevFolder))
  .pipe( notify( { message: 'TASK: "compile styles" Completed!', onLast: true }))
}));

gulp.task( 'buildtheme',  gulp.series('copyfiles', 'styles', 'buildzip'));
