<?php get_header(); ?>
<div class="row">
	<?php get_sidebar(); ?>
	<div class="col-10 col-md-10 col-lg-10 col-xl-10">
		<div class="row">
			<div class="col-lg-9 col-xl-9">
				<div id="primary" class="content-area">
					<main id="main" class="site-main">
				<?php if ( have_posts() ) : ?>
					<h1 class="page-title"> Suchergebnis </h1>
					<?php while ( have_posts() ) : the_post(); ?>
					<?php
						get_template_part( 'template-parts/content', 'search' );
						?>
						<?php endwhile; ?>
						<?php //the_posts_navigation(); 			?>
					<?php else : ?>
						<?php get_template_part( 'template-parts/content', 'none' ); ?>
					<?php endif; ?>
				</main><!-- #main -->
			</div><!-- #primary -->
		</div><!-- #col -->
		<?php get_sidebar('second'); ?>
	</div><!-- #row -->
</div><!--col-md-8 col-xs-12 -->
</div> <!-- #row -->

<?php get_footer(); ?>
