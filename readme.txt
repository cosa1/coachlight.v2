=== Coachlight v2 ===
Contributors: (this should be a list of wordpress.org userid's)
Donate link: 
Tags: theme, trainingssystem
Requires at least: 5.1
Tested up to: 5.9
Stable tag: 4.3
License: GPLv3 or later
License URI: https://www.gnu.org/licenses/gpl-3.0.html

Here is a short description of the plugin.  This should be no more than 150 characters.  No markup here.

== Description ==

responsive template for coaching platform

== Installation ==

install to themes section





== Changelog ==

= 1.2.8.1 =
* Kleinere Anpassungen

= 1.2.8 =
* Styles für Hervorhebung von aktiven Menü-Elementen ergänzt

= 1.2.7 =
* Styles für ziffernlose Darstellung der Seitennummerierung ergänzt

= 1.2.6 =
* Erweiterung Styles Arbeitsplatz-Check für Training "Gesunde Arbeitsumgebung" (Care4Care-Projekt)
* Alert-Farbe gesetzt (Jolanda-Projekt)

= 1.2.5 =
* Erweiterung Styles Arbeitsplatz-Check (Care4Care)

= 1.2.4 =
* Anpassungen Jolanda
* Neuer Style iSleep
* Kleine Verbesserungen

= 1.2.3 =
* Styles Jolanda-Check
* Farbanpassungen und Bugfixes
* Farben für TS-Feature Kategorien bewerten

= 1.2.2 =
* Styles Gesundheits-Check (Care4Care)
* Hinterlegten border-radius des Themes eingelesen
* border-color für jedes Theme hinzugefügt

= 1.2.1 =
* Style Anpassungen Dashboard
* Styles Arbeitsplatz-Check (Care4Care)

= 1.2.0 =
* Neuer Style: "Get On"
* Slider-Styles an Themes angepasst
* Mobile Styles für Landingpage-Image überarbeitet

= 1.1.9 =
* Style für Pagination der Nutzerliste
* Style für Überschriften in Alerts angepasst
* Farben für Links an Design angepasst
* company-tables überarbeitet
* Erweiterungen für Checks
* Anpassungen für Trainingsübersicht als Tabelle

= 1.1.8 =
* Anpassungen an die Übungen
* Kleine Verbesserungen

= 1.1.7 =
* Design Anpassungen für Links und Icons
* Neuer Style: "Jolanda"

= 1.1.6 =
* Landingpage kann nun mit eigenem Bild versehen werden. 
* Neuer Style: "Digisounds"

= 1.1.5 =
* Accordion font-size ins TS verschoben
* Schriftgrößen einheitlich für p, ul und ol
* Templates für kleine, mittlere und große Ränder

= 1.1.4 =
* Schmaler Contentbereich
* Passwortvorschlag unterbunden

= 1.1.3 =
* Fehlerbehebung Banner-Bilder gehen nicht über die gesamte Breite

= 1.1.2 =
* Schmalerer Contentbereich
* Kleine Verbesserungen und Erweiterungen

= 1.1.1 = 
* Kleinere Verbesserungen

= 1.1.0 =
* Bootstrap 3 wurde auf Vesion 4 aktualisiert. Siehe Changelog des Trainingssystems
* Anpassung des AOK-Styles
* Kleine Verbesserungen und Fehlerbehebungen

= 1.0.1 =
* Kleine Verbesserungen und Erweiterungen

= 1.0.0 =
* Auswahl von verschiedenen Styles über den Customizer
* Updates über den Update-Server

= 0.0.3 =
* Pfad für Poster-Tag im video sind nun auf die jeweilige Installation bezogen


= 0.0.2 =
* video links sind nun auf die jeweilige Installation bezogen






