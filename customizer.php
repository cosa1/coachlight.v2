<?php

function load_customize_classes( $wp_customize ) {  

  require get_template_directory() . '/customizer-alpha-color-picker/class-customizer-alpha-color-control.php';
}
add_action( 'customize_register', 'load_customize_classes', 0);

function cosa_customize_register( $wp_customize ) {

    global $theme_styles;
    $selected_theme = get_theme_mod('style_selection');

    // Not used in template, so remove it
    $wp_customize->remove_control('header_textcolor');

    // Size section
    $wp_customize->add_section( 'coachlight_size_section' , array(
        'title'      => 'Größe',
        'priority'   => 50,
    ) );

    // Navbar size
    $wp_customize->add_setting(
        'header_navbar_size',
        array(
            'default'         => '50'
        )
    );

    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'header_navbar_size',
            array(
                'label'           => 'Navbar Größe (in px)',
                'description'     => 'Default: 50px',
                'section'         => 'coachlight_size_section',
                'settings'        => 'header_navbar_size',
                'type'            => 'number',
                'input_attrs'     => array(
                    'min'   => 50,
                    'step'  => 1
                )
            )
        )
    );

    // Footer size
    $wp_customize->add_setting(
        'footer_size',
        array(
            'default'         => '281'
        )
    );

    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'footer_size',
            array(
                'label'           => 'Footer Größe (in px)',
                'description'     => 'Default: 281px',
                'section'         => 'coachlight_size_section',
                'settings'        => 'footer_size',
                'type'            => 'number',
                'input_attrs'     => array(
                    'min'   => 0,
                    'step'  => 1
                )
            )
        )
    );

    $wp_customize->add_setting( 'landingpage_cover_img', true );

    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'landingpage_cover_img',
            array(
                'label'         => 'Coverbild für das Template Landing-Page anzeigen',
                'section'       => 'header_image',
                'type'           => 'checkbox',
            )
          )
      );

    $wp_customize->add_setting( 'landingpage_cover_img_file', array(

    ));
 
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'landingpage_cover_img_file', array(
        'label' => 'Abweichendes Startseitenbild',
        'description' => "Hier kann ein großflächiges Bannerbild eingerichtet werden, welches das Standardbild des Themes überschreibt.",
        'priority' => 20,
        'section' => 'header_image',
        'settings' => 'landingpage_cover_img_file',
    )));

    // Style Selection Dropdown
    $wp_customize->add_setting(
        'style_selection',
        array(
            'default' => sizeof($theme_styles) > 0 ? array_keys($theme_styles)[0] : '',
        )
    );
    
    $select_array = array();
    foreach($theme_styles as $stylename => $style) {
        $select_array[$stylename] = $style['name'];
    }

    $wp_customize->add_control( 
        new WP_Customize_Control(
            $wp_customize,
            'style_selection',
            array(
                'label'      => "Style des Themes auswählen",
                'description' => "Durch Auswahl eines anderen Styles werden die Farben des Themes geändert.<br>Diese Auswahl überschreibt <b>nicht</b> die in den folgenden Einstellungen gesondert gesetzten Hintergrundfarben für Header, Body und Footer.",
                'settings'   => 'style_selection',
                'priority'   => 10,
                'section'    => 'colors',
                'type'    => 'select',
                'choices' => $select_array,
            )
        ) 
    );

    // Colors
    if( class_exists('Customizer_Alpha_Color_Control')){
      
      // Navbar-Header background color
      $wp_customize->add_setting(
          'header_bg_color',
          array(
                // 'default'     => 'rgba(255,255,255,0.2)'
          )
      );
      $wp_customize->add_control(
          new Customizer_Alpha_Color_Control(
              $wp_customize,
              'header_bg_color',
              array(
                  'label'         => 'Hintergrundfarbe des Headers',
                  'section'       => 'colors',
                  'settings'      => 'header_bg_color',
                  'show_opacity'  => true, 
                  'palette'	      => false
              )
            )
        );

        // Navbar-Header font color
        $wp_customize->add_setting(
            'header_font_color',
            array(
                //   'default'     => 'rgba(10,0,2,0.98)'
            )
        );
        $wp_customize->add_control(
            new Customizer_Alpha_Color_Control(
                $wp_customize,
                'header_font_color',
                array(
                    'label'         => 'Schriftfarbe des Headers',
                    'section'       => 'colors',
                    'settings'      => 'header_font_color',
                    'show_opacity'  => true, 
                    'palette'	      => false
                )
              )
          );

      // Body background color
      $wp_customize->add_setting(
          'body_bg_color',
          array(
                'default'     => 'rgba(255,255,255,0)'
          )
      );
      $wp_customize->add_control(
          new Customizer_Alpha_Color_Control(
              $wp_customize,
              'body_bg_color',
              array(
                  'label'         => 'Hintergrundfarbe des Bodys',
                  'section'       => 'colors',
                  'settings'      => 'body_bg_color',
                  'show_opacity'  => true, 
                  'palette'	      => false
              )
            )
        );

      // Footer background color
      $wp_customize->add_setting(
          'footer_bg_color',
          array(
                // 'default'     => 'rgba(255,255,255,0)'
          )
      );
      $wp_customize->add_control(
          new Customizer_Alpha_Color_Control(
              $wp_customize,
              'footer_bg_color',
              array(
                  'label'         => 'Hintergrundfarbe des Footers',
                  'section'       => 'colors',
                  'settings'      => 'footer_bg_color',
                  'show_opacity'  => true, 
                  'palette'	      => false
              )
            )
        );
  }
}
add_action( 'customize_register', 'cosa_customize_register' );