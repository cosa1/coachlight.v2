<?php
if ( ! is_active_sidebar( 'footer-1' ) ) {


	return;
}
?>


<div class="">
	<div id="partner" class="widget-area" role="complementary">
		<?php dynamic_sidebar( 'footer-1' ); ?>
	</div><!-- #secondary -->
</div><!-- .col-md-4>-->
