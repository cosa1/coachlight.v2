<?php get_header(); 

/*
 * Template Name: Large Margins
 * Template Post Type: page, Seiten
 == Standardtemplate
 * Description: Template for displaying centered content with wide margins. Suitable for a trainings page.
 */
?>
<div class="row">
	<?php get_sidebar(); ?>
	<div class="col-12 col-md-12 col-lg-12 col-xl-12">
		<div class="row">
			<div class="offset-lg-1 col-lg-10 offset-xl-3 col-xl-6">
				<div id="primary" class="content-area">
					<main id="main" class="site-main">
				<?php /*if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post(); ?>
					<?php
						get_template_part( 'template-parts/content', get_post_format() );
						?>
						<?php endwhile; ?>
						<?php //the_posts_navigation(); 			?>
					<?php else : ?>
						<?php get_template_part( 'template-parts/content', 'none' ); ?>
					<?php endif; */?>

                        <?php if ( have_posts() ) : ?>
                            <?php while ( have_posts() ) : the_post(); ?>
                                <?php the_content();
																//coach_output_pageview(); ?>
                            <?php endwhile; ?>
                        <?php endif; ?>
				</main><!-- #main -->
			</div><!-- #primary -->
		</div><!-- #col -->
		<?php get_sidebar('second'); ?>
	</div><!-- #row -->
</div><!--col-md-8 col-xs-12 -->
</div> <!-- #row -->

<?php get_footer(); ?>
