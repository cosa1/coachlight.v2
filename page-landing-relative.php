<?php
/*
 * Template Name: Landing Page
 == Standardtemplate
 * Description: Page template without sidebar
 */
get_template_part('template-parts/header','landing-relative'); ?>

				<div id="primary" class="content-area">
					<main id="main" class="site-main">


								<?php //the_content(); ?>
								<?php if ( have_posts() ) : ?>
									<?php while ( have_posts() ) : the_post(); ?>
										<?php get_template_part( 'template-parts/content', 'landing-relative' ); ?>
										<?php
											// If comments are open or we have at least one comment, load up the comment template.
											if ( comments_open() || get_comments_number() ) :
												comments_template();
											endif;
										?>
									<?php endwhile; // End of the loop. ?>
								<?php endif; ?>
					</main>
				</div>
<?php get_sidebar('footer-1');?>
<?php get_footer(); ?>
